# Environment Modules - Managing Software on Shared Systems

## Problem
HPC systems are utilized by a large number of users with diverse software demands. These users
often require different versions of a software package or even competing packages with similar
or overlapping functionality.

For example, the meterological service used to run weather forecast simulations for a
very long time on Vilje. They needed two sets of software:

1. **Production**: The software versions needed for producing the next weather forecast.
2. **Development**: The software versions for the next generation of the code, testing it
  with the new software versions to see whether it still produces the correct results.

## Solution

Provide a mechanism that enables users to configure their software environment according
to their needs.

Environment modules provide such a mechanism, enabling users to:

1. Load and unload specific software versions.
2. Switch between different software packages.
3. Save often used packages in collections.


## Listing, Loading, and Unloading of Software

The ```module``` command is the primary way of managing software packages on Idun. All its options
and subcommands wtih explanations can be listed using ```module --help```. In this section, we
only focus on the following subcommands:

* **avail**: List available modules that can be loaded
* **list**: List all available modules
* **load**: Load module(s)
* **unload**: Unload module(s)
* **purge**: Unloads all loaded modules

After login into Idun, no software module is loaded. The ```list``` subcommand can be used to
verify this:

```
>$ module list

No modules loaded
```

The ```avail``` subcommand can be used to see the currently available software modules for loading:
```
>$ module avail

------------------------------------------------------------------------------------------ /share/apps/modules/all/Core --------------------------------------------------------------------------------
   ADF/2018.103.pc64_linux.intelmpi        Eigen/3.3.4                  Go/1.8.1                   foss/2017a            gompic/2018a                           ifort/2018.3.222-GCC-7.3.0-2.30
   CFX/19.2                                FLUENT/18.0                  Java/1.8.0_92              foss/2017b            gompic/2018b                    (D)    ifort/2019.1.144-GCC-8.2.0-2.31.1 (D)
   COMSOL/5.3a                             FLUENT/18.2                  Java/1.8.0_162             foss/2018a            goolfc/2017b                           intel/2017a
   CUDA/8.0.61                             FLUENT/19.2           (D)    Java/1.8.0_192      (D)    foss/2018b            icc/2017.1.132-GCC-6.3.0-2.27          intel/2017b
   CUDA/9.0.176                            GCC/4.9.3-2.25               MATLAB/2016b               foss/2019a     (D)    icc/2017.4.196-GCC-6.4.0-2.28          intel/2018a
   CUDA/9.1.85                             GCC/5.4.0-2.26               MATLAB/2017a               fosscuda/2018a        icc/2018.1.163-GCC-6.4.0-2.28          intel/2018b
   CUDA/10.0.130                    (D)    GCC/6.3.0-2.27               MATLAB/2018a        (D)    fosscuda/2018b (D)    icc/2018.3.222-GCC-7.3.0-2.30          intel/2019a                       (D)
   EasyBuild/3.6.2                         GCC/6.4.0-2.28        (D)    Maple/2018.0               gcccuda/2017b         icc/2019.1.144-GCC-8.2.0-2.31.1 (D)    iomkl/2018a
   EasyBuild/3.7.0                         GCC/7.3.0-2.30               STAR-CCM+/13.02.011        gcccuda/2018a         ifort/2017.1.132-GCC-6.3.0-2.27        iomkl/2018b                       (D)
   EasyBuild/3.8.0                         GCC/8.1.0-CUDA-9.1.85        foss/2016a                 gcccuda/2018b  (D)    ifort/2017.4.196-GCC-6.4.0-2.28
   EasyBuild/3.8.1                  (D)    GCC/8.2.0-2.31.1             foss/2016b                 gompic/2017b          ifort/2018.1.163-GCC-6.4.0-2.28

------------------------------------------------------------------------------------- /share/apps/lmod/lmod/modulefiles/Core ----------------------------------------------------------------------------
   lmod    settarg

  Where:
   D:  Default Module

Use "module spider" to find all possible modules.
Use "module keyword key1 key2 ..." to search for all possible modules matching any of the "keys".
```

Let's load the MATLAB/2018a module, using the ```load``` subcommand:
```
>$ module load MATLAB/2018a

>$ module list

Currently Loaded Modules:
  1) MATLAB/2018a

```

After loading the MATLAB/2018a module, the ```matlab``` command is available for use:
```
>$ matlab

MATLAB is selecting SOFTWARE OPENGL rendering.

                                                                                         < M A T L A B (R) >
                                                                               Copyright 1984-2018 The MathWorks, Inc.
                                                                                R2018a (9.4.0.813654) 64-bit (glnxa64)
                                                                                          February 23, 2018


To get started, type one of these: helpwin, helpdesk, or demo.
For product information, visit www.mathworks.com.

>>
```

We can remove a loaded module again by using the ```unload``` subcommand:
```
>$ module list

Currently Loaded Modules:
  1) MATLAB/2018a

>$ module unload MATLAB/2018a

>$ module list

No modules loaded
```

Alternatively, we can remove all loaded modules by using the ```purge``` subcommand:
```
>$ module list

Currently Loaded Modules:
  1) GCCcore/.6.4.0   2) binutils/.2.28   3) GCC/6.4.0-2.28   4) Java/11.0.2

>$ module purge

>$ module list

No modules loaded
```

### A Closer Look at ```module avail```
The following example shows that the output of ```module avail``` depends on what modules are
loaded:
```
>$ module list

No modules loaded

>$ module avail

-------------------------------------------------------------------------------------------
   ABAQUS/2018-hotfix-1904                 CUDA/8.0.61            FLUENT/19.3           (D)
   ADF/2018.103.pc64_linux.intelmpi        CUDA/9.0.176           GCC/4.9.3-2.25
   Anaconda3/2018.12                       CUDA/9.1.85            GCC/5.4.0-2.26
   Ansys/19.3                              CUDA/10.0.130          GCC/6.3.0-2.27
   AnsysEM/19.3                            CUDA/10.1.105   (D)    GCC/6.4.0-2.28        (D)
   AnsysEM/19.4                            EasyBuild/4.1.1        GCC/7.3.0-2.30
   AnsysEM/20.1                     (D)    Eigen/3.3.4            GCC/8.1.0-CUDA-9.1.85
   CFX/19.2                                Eigen/3.3.7     (D)    GCC/8.2.0-2.31.1
   CMake/3.12.1                            FLUENT/18.0            GCC/8.3.0
   COMSOL/5.3a                             FLUENT/18.2            GPAW-setups/0.9.9672
   COMSOL/5.4                       (D)    FLUENT/19.2            Go/1.8.1

-------------------------------------------------------------------------------------------
   lmod    settarg

  Where:
   D:  Default Module

Use "module spider" to find all possible modules.
Use "module keyword key1 key2 ..." to search for all possible modules matching any of the "keys". 

>$ module load GCC

>$ module list

Currently Loaded Modules:
  1) GCCcore/.6.4.0   2) binutils/.2.28   3) GCC/6.4.0-2.28

>$ module avail

-------------------------------------------------------------------------------------------------------------------- /share/apps/modules/all/Compiler/GCC/6.4.0-2.28 --------------------------------------------------------------------------------------------------------
   CUDA/9.0.176    CUDA/9.1.85    Clang/5.0.1    GSL/2.4    METIS/5.1.0    OpenBLAS/0.2.20    OpenMPI/2.1.1    OpenMPI/2.1.2 (D)

--------------------------------------------------------------------------------------------------------------------- /share/apps/modules/all/Compiler/GCCcore/6.4.0 ---------------------------------------------------------------------------------------------------------
   Bazel/0.11.0    Bazel/0.11.1 (D)    CMake/3.10.0    ImageMagick/7.0.7-24    ImageMagick/7.0.7-28 (D)    Perl/5.26.0    Perl/5.26.1 (D)    Python/2.7.14-bare    git/2.14.1

------------------------------------------------------------------------------------------------------------------------------ /share/apps/modules/all/Core ------------------------------------------------------------------------------------------------------------------
   ABAQUS/2018-hotfix-1904                 CUDA/8.0.61            FLUENT/19.3           (D)      Java/1.8.0_92                   PGI/19.10-GCC-8.3.0        foss/2019a            git-lfs/2.7.1                        icc/2019.1.144-GCC-8.2.0-2.31.1   (D)    intel/2019a
   ADF/2018.103.pc64_linux.intelmpi        CUDA/9.0.176           GCC/4.9.3-2.25                 Java/1.8.0_162                  STAR-CCM+/12.02.010        foss/2019b     (D)    gompic/2017b                         iccifort/2019.5.281                      intel/2019b (D)
   Anaconda3/2018.12                       CUDA/9.1.85            GCC/5.4.0-2.26                 Java/1.8.0_192                  STAR-CCM+/13.02.011 (D)    fosscuda/2018a        gompic/2018a                         ifort/2017.1.132-GCC-6.3.0-2.27          iomkl/2018a
   Ansys/19.3                              CUDA/10.0.130          GCC/6.3.0-2.27                 Java/11.0.2              (D)    Stata/15                   fosscuda/2018b        gompic/2018b                         ifort/2017.4.196-GCC-6.4.0-2.28          iomkl/2018b (D)
   AnsysEM/19.3                            CUDA/10.1.105   (D)    GCC/6.4.0-2.28        (L,D)    Julia/1.3.1-linux-x86_64        cuDNN/7.6.4.38             fosscuda/2019a        gompic/2019a                         ifort/2018.1.163-GCC-6.4.0-2.28
   AnsysEM/19.4                            EasyBuild/4.1.1        GCC/7.3.0-2.30                 MATLAB/2016b                    foss/2016a                 fosscuda/2019b (D)    gompic/2019b                  (D)    ifort/2018.3.222-GCC-7.3.0-2.30
   AnsysEM/20.1                     (D)    Eigen/3.3.4            GCC/8.1.0-CUDA-9.1.85          MATLAB/2017a                    foss/2016b                 gcccuda/2017b         goolfc/2017b                         ifort/2019.1.144-GCC-8.2.0-2.31.1 (D)
   CFX/19.2                                Eigen/3.3.7     (D)    GCC/8.2.0-2.31.1               MATLAB/2018a                    foss/2017a                 gcccuda/2018a         icc/2017.1.132-GCC-6.3.0-2.27        intel/2017a
   CMake/3.12.1                     (D)    FLUENT/18.0            GCC/8.3.0                      MATLAB/2018b                    foss/2017b                 gcccuda/2018b         icc/2017.4.196-GCC-6.4.0-2.28        intel/2017b
   COMSOL/5.3a                             FLUENT/18.2            GPAW-setups/0.9.9672           MATLAB/2019a             (D)    foss/2018a                 gcccuda/2019a         icc/2018.1.163-GCC-6.4.0-2.28        intel/2018a
   COMSOL/5.4                       (D)    FLUENT/19.2            Go/1.8.1                       Maple/2018.0                    foss/2018b                 gcccuda/2019b  (D)    icc/2018.3.222-GCC-7.3.0-2.30        intel/2018b

------------------------------------------------------------------------------------------------------------------------- /share/apps/lmod/lmod/modulefiles/Core --------------------------------------------------------------------------------------------------------------
   lmod    settarg

  Where:
   L:  Module is loaded
   D:  Default Module

Use "module spider" to find all possible modules.
Use "module keyword key1 key2 ..." to search for all possible modules matching any of the "keys".
```

The example illustrates the following aspects of environment modules:

1. **Default Modules**: Packages with different version have often a *default module*. The module
   is marked with ```(D)``` in the output of ```module avail```. Default modules enable a user to
   avoid the specification of a concrete version, as exemplified above with ```module load GCC```.
2. **Loaded Modules**: Modules that are currently loaded are marked in the output of
   ```module avail``` with ```(L)```. This is exemplified in the second output of
   ```module avail``` above: ```GCC/6.4.0-2.38 (L,D)```.
3. **Module Hierarchy**: A lot of software is dependent on the presence of other software in
   order to function correctly. This dependency is reflected in the second output of
   ```module avail``` in the example above. The modules ```Bazel/0.11.0```, ```CMake/3.10.0```,
   ```ImageMagick/7.0.7-24```, ... only appear in the output of ```module avail``` *after* module
   ```GCC/6.4.0``` was loaded. This means that these modules can only be loaded after
   module ```GCC/6.4.0``` was loaded. This loading dependency reflects the dependency of the
   software: ```ImageMagick/7.0.7-24``` depends on ```GCC/6.4.0``` to function correctly, and
   therefore can only be loaded after module ```GCC/6.4.0``` was loaded.

The third point above emphasized the dependency of modules among each other. This
dependency/hierarchy can be illustrated as a simple graph:

![](figures/module-graph.png)

The above graph illustrates the dependency of the individual modules among each other, and the
order in which they need to be loaded.

This begs the question:

If all the dependencies of a module need to be loaded before the module itself becomes visible, how
do I know if a specific module and what versions of it are present? Moreover, assuming I know that
a certain module is present, how do I know its dependencies, *i.e.*, how does one know what modules
need to be loaded before the module itself can be loaded?

## Searching for Software

We can use ```module spider``` to list *all* available software modules:
```
>$ module spider

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
The following is a list of the modules currently available:
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  ABAQUS: ABAQUS/2018-hotfix-1904
    Finite Element Analysis software for modeling, visualization and best-in-class implicit and explicit dynamics FEA.

  ADF: ADF/2018.103.pc64_linux.intelmpi
    ADF is a premium-quality quantum chemistry software package based on Density Functional Theory (DFT).

  ASE: ASE/3.18.0b1-Python-3.6.6
    ASE is a python package providing an open source Atomic Simulation Environment in the Python scripting language.

  Anaconda3: Anaconda3/2018.12
    Built to complement the rich, open source Python community, the Anaconda platform provides an enterprise-ready data analytics platform that empowers companies to adopt a modern open data science analytics architecture.

  Ansys: Ansys/19.3
    ANSYS Mechanical uses finite element analysis (FEA) for structural analysis.

  ...
```

Alternatively, we can search for a specific software module by providing a keyword to
```module spider```:
```
>$ module spider R

---------------------------------------------------------------------------------------------------
  R:
---------------------------------------------------------------------------------------------------
    Description:
      R is a free software environment for statistical computing and graphics.

     Versions:
        R/3.3.1
        R/3.3.3
        R/3.4.3-X11-20171023
        R/3.4.4-X11-20180131
        R/3.5.1
        R/3.6.0
        R/3.6.2

     Other possible modules matches:
        APR  APR-util  Armadillo  FriBidi  GCCcore  GObject-Introspection  GROMACS  GStreamer  ...

---------------------------------------------------------------------------------------------------
  To find other possible module matches do:
      module -r spider '.*R.*'

---------------------------------------------------------------------------------------------------
  For detailed information about a specific "R" module (including how to load the modules) use the
  module's full name.
  For example:

     $ module spider R/3.4.4-X11-20180131
---------------------------------------------------------------------------------------------------
```

Finally, we can inspect the dependencies of a particular module with:
```
>$ module spider R/3.6.0

---------------------------------------------------------------------------------------------------
  R: R/3.6.0
---------------------------------------------------------------------------------------------------
    Description:
      R is a free software environment for statistical computing and graphics.

     Other possible modules matches:
        APR, APR-util, Armadillo, FriBidi, GCCcore, GObject-Introspection, GROMACS, GStreamer, ...

    You will need to load all module(s) on any one of the lines below before the "R/3.6.0" module
    is available to load.

      GCC/8.2.0-2.31.1  CUDA/10.1.105  OpenMPI/3.1.3
      GCC/8.2.0-2.31.1  OpenMPI/3.1.3
      icc/2019.1.144-GCC-8.2.0-2.31.1  impi/2018.4.274
      ifort/2019.1.144-GCC-8.2.0-2.31.1  impi/2018.4.274

    Help:

      Description
      ===========
      R is a free software environment for statistical computing
       and graphics.


      More information
      ================
       - Homepage: http://www.r-project.org/


      Included extensions
      ===================
      abc-2.1, abc.data-1.0, abe-3.0.1, abind-1.4-5, acepack-1.4.1, adabag-4.2,
      ade4-1.7-13, ADGofTest-0.3, aggregation-1.0.1, akima-0.6-2, AlgDesign-1.1-7.3,
      animation-2.6, aod-1.3.1, ape-5.3, arm-1.10-1, askpass-1.1, asnipe-1.1.11,
      assertthat-0.2.1, AUC-0.3.0, audio-0.1-6, b-a, backports-1.1.4, bacr-1.0.1,
      bartMachine-1.2.4.2, bartMachineJARs-1.1, base64-2.0, base64enc-0.1-3,
      BatchJobs-1.8, BayesianTools-0.1.6, bayesm-3.1-1, BayesPen-1.0, BB-2014.10-1,
      BBmisc-1.11, BCEE-1.2, BDgraph-2.59, bdsmatrix-1.3-3, beanplot-1.2,
      beeswarm-0.2.3, BH-1.69.0-1, BiasedUrn-1.07, bibtex-0.4.2, bigmemory-4.5.33,
      bigmemory.sri-0.1.3, bindr-0.1.1, bindrcpp-0.2.2, bio3d-2.3-4, biom-0.3.12,
      bit-1.1-14, bit64-0.9-7, bitops-1.0-6, blob-1.1.1, BMA-3.18.9, bmp-0.3,
      bnlearn-4.4.1, bold-0.8.6, boot-1.3-22, bootstrap-2017.2, Boruta-6.0.0,
      brew-1.0-6, brglm-0.6.2, bridgedist-0.1.0, bridgesampling-0.6-0,
      Brobdingnag-1.2-6, broom-0.5.2, bst-0.3-17, Cairo-1.5-10, calibrate-1.7.2,
      ...

---------------------------------------------------------------------------------------------------
  To find other possible module matches do:
      module -r spider '.*R/3.6.0.*'
```

This shows the necessary dependencies for module ```R/3.6.0```. Now the module can be loaded as
follows:
```
>$ module list

No modules loaded

>$ module load GCC/8.2.0-2.31.1 CUDA/10.1.105 OpenMPI/3.1.3 icc/2019.1.144-GCC-8.2.0-2.31.1 impi/2018.4.274 ifort/2019.1.144-GCC-8.2.0-2.31.1 R/3.6.0

>$ module list

Currently Loaded Modules:
  1) GCCcore/.8.2.0     7) XZ/.5.2.4                        13) impi/2018.4.274                    19) freetype/.2.9.1     25) nettle/.3.4.1     31) pixman/.0.38.0     37) libreadline/.8.0      43) Java/11.0.2         49) ICU/.64.2          55) JasPer/.2.0.14
  2) binutils/.2.31.1   8) libxml2/.2.9.8                   14) ifort/2019.1.144-GCC-8.2.0-2.31.1  20) ncurses/.6.1        26) libdrm/.2.4.97    32) libffi/.3.2.1      38) Tcl/.8.6.9            44) Tk/.8.6.9           50) Szip/.2.1.1        56) LittleCMS/.2.9
  3) GCC/8.2.0-2.31.1   9) libpciaccess/.0.14               15) imkl/2019.1.144                    21) util-linux/.2.33    27) LLVM/7.0.1        33) gettext/.0.19.8.1  39) SQLite/.3.27.2        45) cURL/.7.63.0        51) HDF5/1.10.5        57) ImageMagick/7.0.8-46
  4) CUDA/10.1.105     10) hwloc/.1.11.11                   16) bzip2/.1.0.6                       22) fontconfig/.2.13.1  28) libunwind/.1.3.1  34) PCRE/.8.43         40) NASM/.2.14.02         46) NLopt/.2.6.1        52) UDUNITS/2.2.26     58) R/3.6.0
  5) zlib/.1.2.11      11) OpenMPI/3.1.3                    17) expat/.2.2.6                       23) X11/.20190311       29) Mesa/.19.0.1      35) GLib/.2.60.1       41) libjpeg-turbo/.2.0.2  47) FFTW/3.3.8          53) GSL/2.5
  6) numactl/.2.0.12   12) icc/2019.1.144-GCC-8.2.0-2.31.1  18) libpng/.1.6.36                     24) GMP/.6.1.2          30) libGLU/.9.0.0     36) cairo/.1.16.0      42) LibTIFF/.4.0.10       48) libsndfile/.1.0.28  54) Ghostscript/.9.27
```

The ```spider``` subcommand permits us to search for concrete modules and retrieve their respective
dependencies. However, we still need to load the modules every time we login into the system.
It would be nice if we could save and restore loaded modules and their respective dependencies so
we do not have to retype the same lengthy commands every single time.

## Module Collections
The ```save``` and ```restore``` subcommand permits us to exactly do this:
```
>$ module list

Currently Loaded Modules:
  1) GCCcore/.8.2.0     7) XZ/.5.2.4                        13) impi/2018.4.274                    19) freetype/.2.9.1     25) nettle/.3.4.1     31) pixman/.0.38.0     37) libreadline/.8.0      43) Java/11.0.2         49) ICU/.64.2          55) JasPer/.2.0.14
  2) binutils/.2.31.1   8) libxml2/.2.9.8                   14) ifort/2019.1.144-GCC-8.2.0-2.31.1  20) ncurses/.6.1        26) libdrm/.2.4.97    32) libffi/.3.2.1      38) Tcl/.8.6.9            44) Tk/.8.6.9           50) Szip/.2.1.1        56) LittleCMS/.2.9
  3) GCC/8.2.0-2.31.1   9) libpciaccess/.0.14               15) imkl/2019.1.144                    21) util-linux/.2.33    27) LLVM/7.0.1        33) gettext/.0.19.8.1  39) SQLite/.3.27.2        45) cURL/.7.63.0        51) HDF5/1.10.5        57) ImageMagick/7.0.8-46
  4) CUDA/10.1.105     10) hwloc/.1.11.11                   16) bzip2/.1.0.6                       22) fontconfig/.2.13.1  28) libunwind/.1.3.1  34) PCRE/.8.43         40) NASM/.2.14.02         46) NLopt/.2.6.1        52) UDUNITS/2.2.26     58) R/3.6.0
  5) zlib/.1.2.11      11) OpenMPI/3.1.3                    17) expat/.2.2.6                       23) X11/.20190311       29) Mesa/.19.0.1      35) GLib/.2.60.1       41) libjpeg-turbo/.2.0.2  47) FFTW/3.3.8          53) GSL/2.5
  6) numactl/.2.0.12   12) icc/2019.1.144-GCC-8.2.0-2.31.1  18) libpng/.1.6.36                     24) GMP/.6.1.2          30) libGLU/.9.0.0     36) cairo/.1.16.0      42) LibTIFF/.4.0.10       48) libsndfile/.1.0.28  54) Ghostscript/.9.27

>$ module store R360-collection

Saved current collection of modules to: R360-collection

>$ module purge

>$ module list

No modules loaded

>$ module restore R360-collection

Restoring modules to user's R360-collection

>$ module list

Currently Loaded Modules:
  1) GCCcore/.8.2.0     7) XZ/.5.2.4                        13) impi/2018.4.274                    19) freetype/.2.9.1     25) nettle/.3.4.1     31) pixman/.0.38.0     37) libreadline/.8.0      43) Java/11.0.2         49) ICU/.64.2          55) JasPer/.2.0.14
  2) binutils/.2.31.1   8) libxml2/.2.9.8                   14) ifort/2019.1.144-GCC-8.2.0-2.31.1  20) ncurses/.6.1        26) libdrm/.2.4.97    32) libffi/.3.2.1      38) Tcl/.8.6.9            44) Tk/.8.6.9           50) Szip/.2.1.1        56) LittleCMS/.2.9
  3) GCC/8.2.0-2.31.1   9) libpciaccess/.0.14               15) imkl/2019.1.144                    21) util-linux/.2.33    27) LLVM/7.0.1        33) gettext/.0.19.8.1  39) SQLite/.3.27.2        45) cURL/.7.63.0        51) HDF5/1.10.5        57) ImageMagick/7.0.8-46
  4) CUDA/10.1.105     10) hwloc/.1.11.11                   16) bzip2/.1.0.6                       22) fontconfig/.2.13.1  28) libunwind/.1.3.1  34) PCRE/.8.43         40) NASM/.2.14.02         46) NLopt/.2.6.1        52) UDUNITS/2.2.26     58) R/3.6.0
  5) zlib/.1.2.11      11) OpenMPI/3.1.3                    17) expat/.2.2.6                       23) X11/.20190311       29) Mesa/.19.0.1      35) GLib/.2.60.1       41) libjpeg-turbo/.2.0.2  47) FFTW/3.3.8          53) GSL/2.5
  6) numactl/.2.0.12   12) icc/2019.1.144-GCC-8.2.0-2.31.1  18) libpng/.1.6.36                     24) GMP/.6.1.2          30) libGLU/.9.0.0     36) cairo/.1.16.0      42) LibTIFF/.4.0.10       48) libsndfile/.1.0.28  54) Ghostscript/.9.27
```

Finally, we can list all the saved collections and inspect the modules of a particular collection
with the ```savelist``` and ```describe``` subcommand:
```
>$ module savelist

Named collection list :
  1) R360-collection

>$ module describe R360-collection

Collection "R360-collection" contains:
  1) GCCcore/.8.2.0     7) XZ/.5.2.4                        13) impi/2018.4.274                    19) freetype/.2.9.1     25) nettle/.3.4.1     31) pixman/.0.38.0     37) libreadline/.8.0      43) Java/11.0.2         49) ICU/.64.2          55) JasPer/.2.0.14
  2) binutils/.2.31.1   8) libxml2/.2.9.8                   14) ifort/2019.1.144-GCC-8.2.0-2.31.1  20) ncurses/.6.1        26) libdrm/.2.4.97    32) libffi/.3.2.1      38) Tcl/.8.6.9            44) Tk/.8.6.9           50) Szip/.2.1.1        56) LittleCMS/.2.9
  3) GCC/8.2.0-2.31.1   9) libpciaccess/.0.14               15) imkl/2019.1.144                    21) util-linux/.2.33    27) LLVM/7.0.1        33) gettext/.0.19.8.1  39) SQLite/.3.27.2        45) cURL/.7.63.0        51) HDF5/1.10.5        57) ImageMagick/7.0.8-46
  4) CUDA/10.1.105     10) hwloc/.1.11.11                   16) bzip2/.1.0.6                       22) fontconfig/.2.13.1  28) libunwind/.1.3.1  34) PCRE/.8.43         40) NASM/.2.14.02         46) NLopt/.2.6.1        52) UDUNITS/2.2.26     58) R/3.6.0
  5) zlib/.1.2.11      11) OpenMPI/3.1.3                    17) expat/.2.2.6                       23) X11/.20190311       29) Mesa/.19.0.1      35) GLib/.2.60.1       41) libjpeg-turbo/.2.0.2  47) FFTW/3.3.8          53) GSL/2.5
  6) numactl/.2.0.12   12) icc/2019.1.144-GCC-8.2.0-2.31.1  18) libpng/.1.6.36                     24) GMP/.6.1.2          30) libGLU/.9.0.0     36) cairo/.1.16.0      42) LibTIFF/.4.0.10       48) libsndfile/.1.0.28  54) Ghostscript/.9.27
```
