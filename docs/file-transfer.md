## Transferring Data from and to Idun

Input data is needed in order to perform useful computations. This data needs to be transferred
to and from Idun.

## Working Directories and Quota

Every user is the owner of a home and work directory on Idun: 

* Home directory: ```/home/<username>```
* Work directory: ```/lustre1/work/<username>```

The home directory is the current directory after login. The work directory should be used as the
main directory for the input/output data of computations. In order to avoid the over-utilization
of storage by a single user, both directories are limited in the size of usable disk space and
number of files by a disk quota. The quota can be check as follows: 

```
>$ lfs quota -u <username> /lustre1

Filesystem  kbytes    quota  limit       grace  files   quota  limit    grace
/lustre1    41220664  0      1000000000  -      289932  0      2000000  -
```

The lfs command prints the used disk space and its limit in kilobytes . In the above example, the
user occupies ca. 41GB of disk space and owns 289932 files. The user's limits are set to a maximum
of 1TB of usable disk space and 2000000 files.

**Note**: Neither the home nor the work directory are backed up.

## File Transfer

Idun has two samba servers, ```idun-samba1.hpc.ntnu.no``` and ```idun-samba2.hpc.ntnu.no```, that
enable the mounting of a user's home and work directory directly to its local machine. The
respective paths to the shares are:

* Home directory: ```//<samba-server>/<username>```
* Work directory: ```//<samba-server>/work```

### Windows

In Windows, the file explorer can be used to attach the home/work directory as a network drive.
Navigate to *File Explorer -> Map network drive* and enter the details as shown below:

![](figures/windows.png)

Replace ```<username>``` with your Idun user. This mounts the home directory as drive ```Z```
into the File Explorer.
