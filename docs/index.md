# Indecol HPC workshop

This material was created by Nico Reissmann for the [Industrial Ecology](https://www.ntnu.edu/indecol/industrial-ecology-programme)
 workshop on HPC basic usage.

Run by [IEDL - *Industrial Ecology Digital Lab*](https://iedl.no)

## Agenda

* [Introduction](introduction.md)
* [Idun](idun.md)
* [Login](login.md)
* [File Transfer](file-transfer.md)
* [Modules](modules.md)
* [Slurm](slurm.md)

## License
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.