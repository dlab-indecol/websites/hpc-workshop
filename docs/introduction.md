# An Introduction to High Performance Computing

## About NTNU's High Performance Computing (HPC) Group

Responsible for NTNU's and Norway's HPC resources

Main activities:
* Procurement and maintenance of NTNU's and Norway's HPC infrastructure
* Scientific computing and researcher support
* HPC research
* ...

We play with the largest, fastest, newest ("state-of-the-art"), and most expensive machines
in Norway.

## What is High Performance Computing? 

High Performance Computing most generally refers to the practice of aggregating computing power in
a way that delivers much higher performance than one could get out of a typical desktop computer or
workstation in order to solve large problems in science, engineering, or business.
<sup>[1](https://insidehpc.com/hpc-basic-training/what-is-hpc/)</sup>

Or as I would put it:
Connect individual computers such that we can use them together to solve compute-intensive
problems.

*What exactly does this mean?*

### Modern Computers

Main components of a computer:

1. Processor with a number of cores
2. Main memory
3. Graphic Processing Units (GPUs)

![](figures/processor.png)

### Solving a Scientific Problem


**Problem:** I want to model the flow of water passing a wedge using
a Lattice-Boltzmann Method.

LBM consists of a series of time steps, where each time step is composed from two kernel
functions: *collision* and *streaming*.

Collision:

![](figures/lbm-collision-phase.png)

Each point has a distribution of densities in six directions, and the collision phase relaxes these
densities towards equilibrium.

Streaming:

![](figures/lbm-streaming-phase.png)

Propagates the densities to the six neighbors of a point.

#### First attempt

![](figures/lbm_domain.png)

All right, physics tells us that Moffatt vortices will form in the wedge. We use 256 x 256 grid and
perform 10000 iterations in total. This took us 77.5 seconds. However, of our 4 cores, we are
only utilizing a single core.

#### Second attempt

![](figures/lbm_domain_04.png)

Ah, that is better. We utilize all four cores of our machine now. It took us 27.4 seconds to
solve the same problem. However, I still believe that this takes too long.

#### Third attempt

![](figures/lbm_domain_16.png)

Great, now we can solve our problem in 8.1 seconds.

#### ...and if we push a little bit more

**Pipe with wedge** (1000 x 1000 grid): http://folk.ntnu.no/janchris/moffatt_web.mp4

**Cylindrical obstruction** (1500 x 1000 grid):
http://folk.ntnu.no/janchris/cylinder_web.mp4

1 core: 11:04:10

112 cores (4 nodes with 28 cores each): 00:07:46

#### How does it scale?

![](figures/LBPM_comparisons.png)

#### Take-away points

1. A lot of problems are so computationally expensive that we need multiple machines to obtain
  results in reasonable time.
2. The problem is divided between the machines, and each machine solves a sub-problem by
  performing the necessary computations *and* communicating with the other machines.
3. Only software with a sufficiently parallelizable part is suitable.

## European HPC Pyramid

![](figures/hpc-pyramid.png)

* Private computers: 1 node, 4 - 20 cores, 1 GPU, 50 kNOK
* Regional facilities: 10 - 250 nodes, 100 - 10000 cores, 100s of GPUs, 1 - 50 MNOK
* National facilities (Betzy): 1344 nodes, 172032 cores, 0 GPUs, 100 MNOK hardware, 70-100 MNOK
  running costs during lifetime
* Continental facilities: ? nodes, ? cores, 90% GPUs, 1 BNOK running costs during lifetime
