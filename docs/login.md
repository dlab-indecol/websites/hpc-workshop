# Logging in to Idun

Users can log in to Idun using an SSH client and one of its three login nodes:
*idun-login1.hpc.ntnu.no*, *idun-login2.hpc.ntnu.no*, and *idun-login3.hpc.ntnu.no*.

## Linux

Open a terminal and use the following command to log in to Idun:

```
>$ ssh -l <username> idun-login1.hpc.ntnu.no

<username>@idun-login1.hpc.ntnu.no's password:
```

Please replace ```<username>``` with your NTNU user name. If you wish to use another login node,
then please replace ```idun-login1``` with either ```idun-login2``` or ```idun-login3```.

## Windows

Windows does not natively ship with an SSH client, and a third party client, such as
![PuTTY](https://www.putty.org/), is required.

### PuTTY
After invoking PuTTY, the user is greeted with the following screen:

![](figures/putty1.svg)

Enter ```idun-login1.hpc.ntnu.no``` (or any of the other login nodes) into the field
*Host Name (or IP address)* and press *Open*. PuTTY should ask you now for your login details in
order to acces the login node:

![](figures/putty2.svg)

Enter your NTNU user name and your password and you should be logged in to the node.
