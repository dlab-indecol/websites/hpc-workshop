# Idun Cluster

Idun is a Tier 2 cluster at NTNU. It is a project between NTNU's faculties and the
IT division to provide a high-availability and professionally administered compute platform
for NTNU. While the IT division provides the backbone of the cluster, such as switches for
high-speed interconnection, storage, and provisioning servers, the individual
faculties/departments provide the compute resources.

Currently, it consists of:

* 11 shareholders - 1 faculty, 8 departments, 2 research groups
* 77 compute nodes - ca. 2000 CPU cores
* 7 admin nodes - 3 login nodes, 2 samba servers, 1 slurm controller, 1 provisioning server
* 5 lustre file system nodes
* 1 test node
* 100 GPUs - 54 NVIDIA P100 16Gb, 26 NVIDIA V100 32Gb, 20 NVIDIA V100 16Gb

In total, the investment in Idun is ca. 25 MNOK.

## Cluster Components

Every cluster consists of three major components:

1. Nodes for computation and administration
2. High-throughput and low latency interconnect
3. Parallel file system

![](figures/idun-topology.svg)

## Cluster Workflow 

The general workflow on a cluster is as follows:

1. ![Login](login.md)
2. ![File transfer](file-transfer.md)
3. ![Loading software](modules.md)
4. ![Running jobs](slurm.md)
